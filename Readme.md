### Introduction to High Performance Computing
#### Autumn, 2017

#### Announcements (see [course webpage](https://imperialhpsc.bitbucket.io) for further information)

14/10/17:
Modified and finalized details of assessment for the class have been posted

13/10/17: 
If you are having trouble using *pip install* during software installation, just use *pip3 install* instead.

Solutions to the first two python exercises have been posted. Please
remember to work through the 3rd and 4th videos/slides before lecture 4 on Monday.

Codes, slides, exercises and solutions are included in the course repo. Syncing your
fork and then updating your clone (using *git pull*) will give you your own copy
of all of these files.

